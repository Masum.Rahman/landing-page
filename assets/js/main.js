(function ($) {
    "use strict";


    /*----------------------------
    jQuery Mean Menu
    ------------------------------ */
    $("#mobile-menu").meanmenu({
        meanMenuContainer: ".mobile-menu",
        meanScreenWidth: "767"
    });
    $(".info-bar").on("click", function () {
        $(".extra-info").addClass("info-open");
    });
    $(".close-icon").on("click", function () {
        $(".extra-info").removeClass("info-open");
    });

    /*----------------------------
    Sticky menu active
    ------------------------------ */
    // // jQuery Count To
    $('.counter-value').each(function(){
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        },{
            duration: 5500,
            easing: 'swing',
            step: function (now){
                $(this).text(Math.ceil(now));
            }
        });
    });

    $('.center').slick({
        centerMode: true,
        dots: true,
        arrows: false,
        centerPadding: '60px',
        // autoplay: true,
        // autoplaySpeed: 2000,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
    var targets = {
        input1: 'input2',
        input2: 'input3',
        input3: 'input4',
        input4: null,
    };

    $(".focus").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $("input.focus").keyup(function (e) {
        if (!(e.target.value != '' && e.target.value >= 0 && e.target.value <= 9)) {
            return;
        }

        var source = e.target.id;
        var target = targets[source];
        if (target) {
            $("#" + target).focus();
        } else {
            $("#" + source).blur();
        }
    });


    $(".div").slice(0, 6).show();
    var i=1;
    $("#loadMore").on('click', function (e) {
        i++;
        if(i==3){
            $( "#loadMore" ).hide();
        }
        e.preventDefault();
        $(".div:hidden").slice(0, 6).slideDown();
        if ($(".div:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('.link-logo').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });


})(jQuery);